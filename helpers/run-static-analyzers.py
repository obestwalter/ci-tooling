#!/usr/bin/python3
import os
import sys
import argparse
import re
from helperslib import BuildSpecs, BuildSystem, CommonUtils, EnvironmentHandler, StaticAnalyzers

# Parse the command line arguments we've been given
parser = argparse.ArgumentParser(description='Utility to run static analyzers on a project.')
parser.add_argument('--product', type=str, required=True)
parser.add_argument('--project', type=str, required=True)
parser.add_argument('--branchGroup', type=str, required=True)
parser.add_argument('--platform', type=str, required=True)
parser.add_argument('--usingInstall', type=str, required=True)
parser.add_argument('--analyzers', type=str, required=True)
parser.add_argument('--targetBranch', type=str, required=False)
parser.add_argument('--baseCommit', type=str, required=False)
arguments = parser.parse_args()

# Load our build specification, which governs how we handle this build
buildSpecification = BuildSpecs.Loader(product=arguments.product, project=arguments.project, branchGroup=arguments.branchGroup, platform=arguments.platform)

# Determine where our source code is checked out to and where builddir is.
# We'll assume that the directory we're running from is where the sources are located
sourcesLocation = os.getcwd()

if arguments.targetBranch:
	baseCommit = StaticAnalyzers.resolveBaseCommitForBranch(sourceDir=sourcesLocation, targetBranch=arguments.targetBranch)
	print("Detected base commit {}".format(baseCommit))
elif arguments.baseCommit and arguments.baseCommit != '0000000000000000000000000000000000000000':
	baseCommit = arguments.baseCommit
else:
	baseCommit = None

buildLocation = CommonUtils.buildDirectoryForSources(sources=sourcesLocation, inSourceBuild=buildSpecification['in-source-build'])

# Prepare analyzers
filesToCheck = StaticAnalyzers.getFilesToCheck(baseCommit=baseCommit, sourceDir=sourcesLocation)
filteredFiles = StaticAnalyzers.removeIgnoredFiles(sourceDir=sourcesLocation, files=filesToCheck)

print('{} changed C++ files to check, {} files ignored'.format(len(filteredFiles), len(filesToCheck) - len(filteredFiles)))

analyzers = list(map(lambda analyzer: StaticAnalyzers.analyzerForName(analyzer), arguments.analyzers.split(',')))

# Run all the analyzers
try:
	ok = StaticAnalyzers.run(sourcesLocation, buildLocation, analyzers, filteredFiles)
except Exception as e:
	raise
	sys.exit(1)

# All analyzers have finished
sys.exit(0 if ok else 1)
