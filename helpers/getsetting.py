#!/usr/bin/python3
import os
import sys
import argparse
import subprocess
from helperslib import BuildSpecs, BuildSystem, CommonUtils, EnvironmentHandler

# Parse the command line arguments we've been given
parser = argparse.ArgumentParser(description='Utility to query for a specific option in build-specs.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--product', type=str, required=True)
parser.add_argument('--project', type=str, required=True)
parser.add_argument('--branchGroup', type=str, default='kf5-qt5')
parser.add_argument('--platform', type=str, required=True)
parser.add_argument('--key', type=str, required=True)
arguments = parser.parse_args()

# Load our build specification, which governs how we handle this build
buildSpecification = BuildSpecs.Loader( product=arguments.product, project=arguments.project, branchGroup=arguments.branchGroup, platform=arguments.platform )
try:
    print(buildSpecification[arguments.key])
except Exception as e:
    print("failed:", e, file=sys.stderr)
    pass
